import unittest
import time
from selenium import webdriver
from selenium.webdriver import ActionChains

from dean_register_page import Register


class TestRegister(unittest.TestCase):

    def test_register(self) -> None:

        driver = webdriver.Chrome(
            executable_path='./chromedriver'
        )
        driver.implicitly_wait(10)
        driver.get("https://develop.dearchnet.com/register")

        #page = TestRegister(driver)
        page = Register(driver)
        page.set_full_name('PyCharm')
        page.set_email('kurchenko+150@dnt-lab.com')
        page.set_password('12022017Test.')
        page.set_repeat_password('12022017Test.')
        check_box = driver.find_element_by_id('acceptTerms')
        ac = ActionChains(driver)
        ac.move_to_element(check_box).click
        ac.perform()

        driver.implicitly_wait(10)
        page.click_register()

        sign_up_button_text = 'Register'

        self.assertEqual('Register', register_button_text)

        driver.close()