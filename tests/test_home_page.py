# Use case: UC19
# Description:
from test_base_page import BasePage
from models.home_page_element import HomePageElement
from typing import List

class HomePage(BasePage):

    def get_navbar(self):
        """
        Menu-dropdown для авторизированого пользователя.
        """
        navbarDropdown4 = self.driver.find_elements_by_id("navbarDropdown4")
        return navbarDropdown4

    def get_current_home_page_elements(self) -> List[HomePageElement]:

        card_elements = self.driver.find_elements_by_class_name('card')

        home_page_elements: List[HomePageElement] = []

        for card_element in card_elements:
            title: str = card_element.find_element_by_class_name('card-body').text
            categories: str = card_element.find_element_by_class_name('info-holder').text
            home_page_element = HomePageElement(title=title, categories=categories)
            home_page_elements.append(home_page_element)

        return home_page_elements

    def click_categories(self) -> None:
        """
        Нажатие по "All categories".
        """
        self.driver.find_element_by_id("navbarDropdown1").click()


    def select_architecture_category(self) -> None:
        """
        Выбор категории "Architecture".
        """
        self.driver.find_element_by_css_selector(".show > .dropdown-item:nth-child(1) .custom-control-label").click()
