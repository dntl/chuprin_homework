import unittest
import time
from selenium import webdriver
from test_login_page import LoginPage
from test_main_menu_dropdown import MainMenuDropDown
from test_home_page import HomePage


class TestLogin(unittest.TestCase):

    def test_login(self) -> None:
        driver = webdriver.Chrome(
            executable_path='./chromedriver'
        )
        # Test name: Login
        # Step # | name | target | value | comment
        # 1 | open | https://develop.dearchnet.com/login |  |
        driver.implicitly_wait(10)
        driver.get("https://develop.dearchnet.com/login")
        login_page = LoginPage(driver)
        main_menu_dropdown = MainMenuDropDown(driver)
        home_page = HomePage(driver)

        login_page.set_email("sokurenko+DeArkNet1@dnt-lab.com")
        login_page.set_password("Test123456")
        login_page.click_login()

        # 2 | assertElementPresent | id=main-menu-dropdown |
        self.assertTrue(len(main_menu_dropdown.get_main_menu_dropdown()) > 0)
        # 3 | assertElementPresent | id=navbarDropdown4 | The Home page is opened(Filter State present)
        self.assertTrue(len(home_page.get_navbar()) > 0)
        driver.close()
