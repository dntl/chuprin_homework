from test_base_page import BasePage


class LoginPage:

    def email_field_element(self):
        """
        Пользователь на станице Log In.
        :param email: Поле email адресс
        """
        return self.driver.find_element_by_id("email")

    def password_field_element(self):
        """
        :param email: Поле пароля
        """
        return self.driver.find_element_by_id("password")

    def set_email(self, email: str):
        """
        :param email: Вводит email адресс
        """
        email_field = self.email_field_element()
        email_field.send_keys(email)

    def set_password(self, password: str):
        """
        :param password: Вводит пароль
        """
        password_field = self.password_field_element()
        password_field.send_keys(password)

    def get_title_page(self) -> str:
        title_page = self.driver.find_element_by_xpath("//h1[contains(.,'Log In to your DeArchNet™ Account')]")
        return title_page.text

    def get_email_title(self) -> str:
        email_title = self.driver.find_element_by_xpath("//label[contains(.,'Email Address *')]")
        return email_title.text

    def get_password_title(self) -> str:
        password_title = self.driver.find_element_by_xpath("//label[contains(.,'Password *')]")
        return password_title.text

    def get_invalid_email_feedback(self) -> str:
        invalid_feedback = self.driver.find_element_by_css_selector(".invalid-feedback > div")
        return invalid_feedback.text

    def click_login(self) -> None:
        """
        Кнопка "Log In" на станице Log In.
        """
        click_login = self.driver.find_element_by_css_selector(".btn")
        click_login.click()

    def get_login_button_text(self) -> str:
        """
        Текст кнопки "Log In" на станице Log In.
        """
        login_button_text = self.driver.find_element_by_css_selector(".btn")
        return login_button_text.text

    def remember_me_button_title(self) -> str:
        """
        Checkbox "Remember Me" на станице Log In.
        """
        remember = self.driver.find_element_by_css_selector(".custom-control-label")
        return remember.text

    def forgot_password_button_title(self) -> str:
        """
        Кнопки "Forgot password?" на станице Log In.
        """
        forgot = self.driver.find_element_by_id("emailHelp")
        return forgot.text
