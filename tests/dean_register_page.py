from test_base_page import BasePage


class Register(BasePage):

    def set_full_name(self, text: str):
        element = self.driver.find_element_by_xpath('//*[@id="main"]/div/app-registration/div/form/div[1]/input').send_keys('PyCharm')

    def set_email(self, text: str):
        element = self.driver.find_element_by_xpath('//*[@id="email"]').send_keys('kurchenko+150@dnt-lab.com')

    def set_password(self, text: str):
        element = self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('12022017Test.')

    def set_repeat_password(self, text: str):
        element = self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('12022017Test.')

    def click_check_box(self) -> None:
        element = self.driver.find_element_by_id('acceptTerms')
        element.click()

    def click_register(self) -> None:
        element = self.driver.find_element_by_xpath('//*[@id="main"]/div/app-registration/div/form/button')
        element.click()

    def register_button_text(self) -> str:
        sign_up_button_text = self.driver.find_element_by_xpath('//*[@id="main"]/div/app-registration/div/form/button')

        return register_button_text
