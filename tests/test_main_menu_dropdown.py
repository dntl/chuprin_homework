from test_base_page import BasePage

class MainMenuDropDown(BasePage):

    def get_main_menu_dropdown(self):
        """
        Menu-dropdown для авторизированого пользователя.
        """
        main_menu_dropdown = self.driver.find_elements_by_id("main-menu-dropdown")
        return main_menu_dropdown
