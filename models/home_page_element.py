from dataclasses import dataclass


@dataclass
class HomePageElement:
    title: str
    categories: str
